# .eslintrc.json

## Rules documentation

- [`eslint`](https://eslint.org/docs/rules/)
- [`eslint-plugin-vue`](https://eslint.vuejs.org/rules/)
- [`eslint-plugin-node`](https://github.com/mysticatea/eslint-plugin-node)